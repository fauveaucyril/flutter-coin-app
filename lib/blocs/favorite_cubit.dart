import 'package:bloc/bloc.dart';
import 'package:coin_app/models/favorite.dart';
import 'package:coin_app/repository/repository.dart';

class FavoriteCubit extends Cubit<List<Favorite>> {
  FavoriteCubit(this._repository) : super([]);
  final Repository _repository;
  void addFavorite(Favorite favorite) {
    emit([...state, favorite]);
    _repository.saveFavorites(state);
  }
  Future<void> loadFavorites() async {
    final List<Favorite> favorites = await _repository.loadFavorites();
    emit(favorites);
  }
}
