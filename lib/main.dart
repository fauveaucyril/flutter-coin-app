import 'package:flutter/material.dart';
import 'package:coin_app/repository/crypto_repository.dart';
import 'package:coin_app/repository/preferences_repository.dart';
import 'package:coin_app/repository/repository.dart';
import 'package:coin_app/ui/app.dart';
import 'package:provider/provider.dart';

import 'blocs/favorite_cubit.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final PreferencesRepository preferencesRepository = PreferencesRepository();
  final CryptoRepository cryptoRepository = CryptoRepository();
  final Repository repository = Repository(cryptoRepository, preferencesRepository);

  final FavoriteCubit favoriteCubit = FavoriteCubit(repository);
  await favoriteCubit.loadFavorites();

  runApp(
      MultiProvider(
        providers: [
          Provider<FavoriteCubit>(create: (_) => favoriteCubit),
          Provider<Repository>(create: (_) => repository),
        ],
        child: const CoinApp(),
      )
  );
}

