import 'dart:convert';

class Crypto {
  String symbol;

  @override
  String toString() {
    return 'Crypto{symbol: $symbol}';
  }

  Crypto(this.symbol);

  String toJson() {
    return jsonEncode({
      'symbol': symbol,
    });
  }
  factory Crypto.fromJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    return Crypto(
      map['symbol'],
    );
  }

  factory Crypto.fromCryptoJson(Map<String, dynamic> json) {
    final String symbol = json['symbol'];

    return Crypto(symbol);
  }

}