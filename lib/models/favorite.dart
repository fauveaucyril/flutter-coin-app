import 'dart:convert';

import 'package:coin_app/models/crypto.dart';

class Favorite {
  int id;
  String name;
  Crypto crypto;

  Favorite(this.id, this.name, this.crypto);

  String toJson() {
    return jsonEncode({
      'id': id,
      'name': name,
      'crypto': crypto.toJson(),
    });
  }
  factory Favorite.fromJson(String json) {
    Map<String, dynamic> map = jsonDecode(json);
    return Favorite(
        map['id'],
        map['name'],
        Crypto.fromJson(map['crypto'])
    );
  }
}