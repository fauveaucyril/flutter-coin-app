class OneCrypto {
  String symbol;
  String buyPrice;
  String sellPrice;
  String maxPrice;
  String minPrice;

  OneCrypto(this.symbol, this.buyPrice, this.sellPrice, this.maxPrice, this.minPrice);

  factory OneCrypto.fromJson(Map<String, dynamic> json) {
    return OneCrypto(
      json['symbol'],
      json['buy'],
      json['sell'],
      json['high'],
      json['low'],
    );
  }
}