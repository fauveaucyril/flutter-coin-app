import 'dart:convert';

import 'package:coin_app/models/one_crypto.dart';
import 'package:http/http.dart';
import 'package:coin_app/models/crypto.dart';

class CryptoRepository {
  Future<List<Crypto>> fetchCryptos(String query) async {
    // final Response response = await get(Uri.https('api-adresse.data.gouv.fr','/search', {'q': query}));
    final Response response = await get(Uri.https('api.kucoin.com','/api/v1/market/stats', {'symbol': query}));
    // final Response listResponse = await get(Uri.https('api.kucoin.com','/api/v1/market/allTickers'));

    if(response.statusCode == 200) {
      final List<Crypto> cryptos = [];

      final Map<String, dynamic> json = jsonDecode(response.body);
      if(json['data']['buy'] != null) {
        cryptos.add(Crypto.fromCryptoJson(json['data']));
      }
      return cryptos;
    } else {
      throw Exception('Failed to load cryptos');
    }
  }

  Future<OneCrypto> fetchSingleCrypto(String query) async {
    final Response response = await get(Uri.https('api.kucoin.com','/api/v1/market/stats', {'symbol': query}));

    final Map<String, dynamic> json = jsonDecode(response.body);

    if(response.statusCode == 200) {
      return OneCrypto.fromJson(json['data']);
    } else {
      throw Exception('Failed to load cryptos');
    }
  }
}