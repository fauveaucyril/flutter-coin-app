import 'package:coin_app/models/favorite.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {

  Future<void> saveFavorites(List<Favorite> favorites) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    final List<String> listJson = [];
    for (final Favorite favorite in favorites) {
      listJson.add(favorite.toJson());
    }

    prefs.setStringList('favorites', listJson);
  }

  Future<List<Favorite>> loadFavorites() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String>? list = prefs.getStringList('favorites');

    if(list == null) {
      return [];
    }

    final List<Favorite> favorites = list.map((element) {
      return Favorite.fromJson(element);
    }).toList();

    return favorites;
  }

}