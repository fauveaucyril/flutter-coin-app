import 'package:coin_app/models/crypto.dart';
import 'package:coin_app/models/favorite.dart';
import 'package:coin_app/models/one_crypto.dart';
import 'package:coin_app/repository/crypto_repository.dart';
import 'package:coin_app/repository/preferences_repository.dart';

class Repository {
  final PreferencesRepository _preferencesRepository;

  Repository(this._cryptosRepository, this._preferencesRepository);

  final CryptoRepository _cryptosRepository;

  Future<List<Crypto>> searchCryptos(String query) {
    return _cryptosRepository.fetchCryptos(query);
  }

  Future<OneCrypto> detailsCrypto(String query) {
    return _cryptosRepository.fetchSingleCrypto(query);
  }

  Future<void> saveFavorites(List<Favorite> favorites) async {
    _preferencesRepository.saveFavorites(favorites);
  }

  Future<List<Favorite>> loadFavorites() async =>_preferencesRepository.loadFavorites();
}