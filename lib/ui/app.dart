import 'package:flutter/material.dart';
import 'package:coin_app/ui/screens/add_favorites.dart';
import 'package:coin_app/ui/screens/home.dart';
import 'package:coin_app/ui/screens/search_crypto.dart';

import 'colors/palette.dart';

class CoinApp extends StatelessWidget {
  const CoinApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: customBlack,
      ),
      routes: {
        '/home': (context) => Home(),
        '/addFavorite': (context) => AddFavorite(),
        '/searchCrypto': (context) => SearchCrypto(),
      },
      initialRoute: '/home',
      // home: Home(),
    );
  }
}