import 'package:flutter/material.dart';

const MaterialColor customBlack = MaterialColor(
  0xff202020,
  <int, Color>{
    50: Color(0xff383838),
    100: Color(0xff333333),
    200: Color(0xff2b2b2b),
    300: Color(0xff262626),
    400: Color(0xff1f1f1f),
    500: Color(0xff1a1a1a),
    600: Color(0xff121212),
    700: Color(0xff0d0d0d),
    800: Color(0xff050505),
    900: Color(0xff000000),
  },
);