import 'package:flutter/material.dart';
import 'package:coin_app/blocs/favorite_cubit.dart';
import 'package:coin_app/models/crypto.dart';
import 'package:coin_app/models/favorite.dart';
import 'package:coin_app/ui/colors/palette.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class AddFavorite extends StatelessWidget {
  AddFavorite({Key? key}) : super(key: key);

  final TextEditingController _nameTextEditingController = TextEditingController();
  final TextEditingController _cryptoTextEditingController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey();
  Crypto? _crypto;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: customBlack,
      appBar: AppBar(
        foregroundColor: Colors.white,
        centerTitle: true,
        elevation: 0,
        shape: const Border(
            bottom: BorderSide(
              color: Colors.white,
            )
        ),
        title: const Text('Ajouter une cryptomonnaie'),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: _nameTextEditingController,
                validator: (String? value) {
                  if(value != null && value.isNotEmpty) {
                    return null;
                  } else {
                    return 'Le champ doit être renseigné';
                  }
                },
                style: const TextStyle(color: Colors.white),
                decoration: const InputDecoration(
                    labelText: 'Nom de votre cryptomonnaie',
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.white),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.green),
                    )),
                cursorColor: Colors.white,
              ),
              const SizedBox(height: 20),
              TextFormField(
                controller: _cryptoTextEditingController,
                validator: (String? value) {
                  if(value != null && value.isNotEmpty) {
                    return null;
                  } else {
                    return 'Le champ doit être renseigné';
                  }
                },
                style: const TextStyle(color: Colors.white),
                decoration: const InputDecoration(
                    labelText: 'Rechercher une cryptomonnaie',
                    labelStyle: TextStyle(
                      color: Colors.white,
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.white),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 1, color: Colors.green),
                    )
                ),
                cursorColor: Colors.white,
                readOnly: true,
                onTap: () async {
                  _crypto = await Navigator.of(context).pushNamed('/searchCrypto') as Crypto;
                  if(_crypto != null) {
                    _cryptoTextEditingController.text = _crypto!.symbol;
                  }
                },
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                style: ButtonStyle(
                  padding: MaterialStateProperty.all<EdgeInsets>(
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 12),
                  ),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50.0),
                          side: const BorderSide(color: Colors.white)
                      )
                  ),
                ),
                onPressed: () {
                  if(_formKey.currentState!.validate() && _crypto != null) {
                    String name = _nameTextEditingController.text;
                    Favorite favorite = Favorite(0, name, _crypto!);
                    Provider.of<FavoriteCubit>(context, listen: false).addFavorite(favorite);
                    Navigator.of(context).pop();
                  }
                },
                child: const Text('ENREGISTRER',style: TextStyle(fontWeight: FontWeight.bold)),
              )
            ],
          ),
        ),
      ),
    );
  }
}
