import 'package:coin_app/models/favorite.dart';
import 'package:coin_app/models/one_crypto.dart';
import 'package:coin_app/repository/crypto_repository.dart';
import 'package:coin_app/repository/preferences_repository.dart';
import 'package:coin_app/repository/repository.dart';
import 'package:coin_app/ui/colors/palette.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DetailsCrypto extends StatelessWidget {
  DetailsCrypto({Key? key, required this.favorite}) : super(key: key);

  final Favorite favorite;
  final Repository repository = Repository(CryptoRepository(), PreferencesRepository());
  late Future<OneCrypto> oneCrypto = repository.detailsCrypto(favorite.crypto.symbol);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: customBlack,
      appBar: AppBar(
        foregroundColor: Colors.white,
        centerTitle: true,
        elevation: 0,
        shape: const Border(
            bottom: BorderSide(
              color: Colors.white,
            )
        ),
        title: Text(favorite.name),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: FutureBuilder<OneCrypto>(
          future: oneCrypto,
          builder: (context, index) {
            if(index.data != null) {
              return Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Text(
                          index.data!.symbol,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                          '\$${index.data!.buyPrice}',
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                            fontWeight: FontWeight.w900,
                          ),
                          textAlign: TextAlign.end,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 30),
                  Column(
                    children: [
                      Row(
                        children: [
                          const Text('24h Max Price:', style: TextStyle(color: Colors.white)),
                          Text(
                            ' \$${index.data!.maxPrice}',
                            style: const TextStyle(
                              color: Colors.green,
                              fontSize: 20,
                            ),
                          ),
                        ]
                      ),
                      const SizedBox(height: 20),
                      Row(
                        children: [
                          const Text('24h Min Price:', style: TextStyle(color: Colors.white)),
                          Text(
                            ' \$${index.data!.minPrice}',
                            style: const TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                            ),
                          ),
                        ]
                      ),
                    ],
                  ),
                ],
              );
            } else {
              return const Text('Impossible de récupérer les données');
            }
          },
        ),
      ),
    );
  }
}
