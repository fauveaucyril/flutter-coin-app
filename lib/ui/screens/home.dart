import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:coin_app/blocs/favorite_cubit.dart';
import 'package:coin_app/models/favorite.dart';
import 'package:coin_app/repository/preferences_repository.dart';
import 'package:coin_app/ui/colors/palette.dart';

import 'details_crypto.dart';

class Home extends StatelessWidget {
  Home({Key? key}) : super(key: key);

  final PreferencesRepository preferencesRepository = PreferencesRepository();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: customBlack,
      appBar: AppBar(
        foregroundColor: Colors.white,
        centerTitle: true,
        elevation: 0,
        shape: const Border(
            bottom: BorderSide(
              color: Colors.white,
            )
        ),
        title: const Text('Mes Favoris'),
      ),
      body: BlocBuilder<FavoriteCubit, List<Favorite>>(
        builder: (context, state) {
          return ListView.separated(
              shrinkWrap: true,
              itemCount: state.length,
              itemBuilder: (BuildContext context, int index) {
                Favorite favorite = state[index];
                return InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailsCrypto(favorite: favorite),
                      )
                    );
                  },
                  child: Container(
                      padding: const EdgeInsets.fromLTRB(0, 5, 20, 0),
                      child: ListTile(
                        textColor: Colors.white,
                        title: Text(favorite.name, style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
                        subtitle: Text(favorite.crypto.symbol, style: const TextStyle(fontSize: 12, fontStyle: FontStyle.italic)),
                        trailing: const Icon(Icons.arrow_forward, color: Colors.green),
                      ),
                      decoration: const BoxDecoration(
                          border: Border(
                              bottom: BorderSide(color: Colors.white)
                          )
                      )
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return const Divider(height: 0);
              }
          );
        }, ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        foregroundColor: customBlack,
        onPressed: () {
          Navigator.of(context).pushNamed('/addFavorite');
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
