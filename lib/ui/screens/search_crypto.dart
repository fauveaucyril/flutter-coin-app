import 'package:coin_app/ui/colors/palette.dart';
import 'package:flutter/material.dart';
import 'package:coin_app/models/crypto.dart';
import 'package:coin_app/repository/crypto_repository.dart';
import 'package:coin_app/repository/preferences_repository.dart';
import 'package:coin_app/repository/repository.dart';

class SearchCrypto extends StatefulWidget {
  SearchCrypto({Key? key}) : super(key: key);

  final Repository repository = Repository(CryptoRepository(), PreferencesRepository());

  @override
  State<SearchCrypto> createState() => _SearchCryptoState();
}

class _SearchCryptoState extends State<SearchCrypto> {
  List<Crypto> _cryptos = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: customBlack,
      appBar: AppBar(
        title: const Text('Rechercher une cryptomonnaie'),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
        child: Column(
          children: [
            TextField(
              onChanged: (value) async {
                if(value.isNotEmpty) {
                  final List<Crypto> cryptos = await widget.repository.searchCryptos(value);
                  setState(() {
                    _cryptos = cryptos;
                  });
                }
              },
              style: const TextStyle(color: Colors.white),
              decoration: const InputDecoration(
                  labelText: 'ex: BTC-USDT',
                  labelStyle: TextStyle(
                    color: Colors.white,
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Colors.white),
                  ),
                  border: UnderlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Colors.white),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(width: 1, color: Colors.green),
                  )),
              cursorColor: Colors.white,
            ),
            Expanded(
              child: ListView.builder(
                  itemCount: _cryptos.length,
                  itemBuilder: (context, index) {
                    final Crypto crypto = _cryptos[index];
                    return ListTile(
                      onTap: () {
                        Navigator.of(context).pop(crypto);
                      },
                      textColor: Colors.white,
                      title: Text(crypto.symbol),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
